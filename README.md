# home_ansible

ansible automation for deployment of stuff around the house

docker - install all the various docker-related things, including setting up the docker ce repo source
python - install python3, pip3, and get them both fully up to date
common - base packages like curl, nmap, vim, etc, that might not be installed by default. this may be silly
golang - install go and potentially set go up for defined users?
updates - just runs some form of apt update && apt upgrade
remoting - install and set up vnc, configure sshd, all the ways you remotely access a nix box
mounts - set up file mounts like nfs mounting off shack2